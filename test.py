import RPi.GPIO as GPIO # Import Raspberry Pi GPIO library
import spidev as SPI
import ST7789
import time
from PIL import Image,ImageDraw,ImageFont

# set previous graph number
prev=1

def print_graph(number):
    disp.clear()
    image = Image.open("graph-" + str(number) + ".jpg")
    disp.ShowImage(image,0,0)
    time.sleep(0.5)

def button_callbackl(channel):
    global prev
    prev=prev-1
    print_graph(prev)
def button_callbackr(channel):
    global prev
    prev=prev+1
    print_graph(prev)

# Raspberry Pi pin configuration:
RST = 27
DC = 25
BL = 24
bus = 0 
device = 0

# 240x240 display with hardware SPI:
disp = ST7789.ST7789(SPI.SpiDev(bus, device),RST, DC, BL)

# Initialize library.
disp.Init()

# Clear display.
disp.clear()

# Create blank image for drawing.
image1 = Image.new("RGB", (disp.width, disp.height), "WHITE")
draw = ImageDraw.Draw(image1)
font = ImageFont.truetype('/usr/share/fonts/truetype/freefont/FreeMonoBold.ttf', 16)
print ("***draw line")
draw.line([(60,60),(180,60)], fill = "BLUE",width = 5)
draw.line([(180,60),(180,180)], fill = "BLUE",width = 5)
draw.line([(180,180),(60,180)], fill = "BLUE",width = 5)
draw.line([(60,180),(60,60)], fill = "BLUE",width = 5)
print ("***draw rectangle")
draw.rectangle([(70,70),(170,80)],fill = "RED")

print ("***draw text")
draw.text((90, 70), 'Corona', fill = "BLUE")
draw.text((90, 120), 'use buttons', fill = "BLUE")
draw.text((90, 140), 'by Jacob S.', fill = "BLUE")
disp.ShowImage(image1,0,0)

#GPIO.setwarnings(False) # Ignore warning for now
#GPIO.setmode(GPIO.BOARD) # Use physical pin numbering
GPIO.setup(16, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) # Set pin 10 to be an input pin and set initial value to be pulled low (off)
GPIO.setup(26, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.add_event_detect(16,GPIO.RISING,callback=button_callbackl) # Setup event on pin 10 rising edge
GPIO.add_event_detect(26,GPIO.RISING,callback=button_callbackr)

message = input("Press enter to quit\n\n") # Run until someone presses enter
GPIO.cleanup() # Clean up
